import React from 'react';
import FormInput from '../../elements/Form/FormInput';
import FormManager from './FormManager';
import Form from '../../elements/Form/Form';
import FormRow from '../../elements/Form/FormRow';
import FormGroup from '../../elements/Form/FormGroup';

class FormBuilder extends React.Component {
  constructor(props) {
    super(props);
  }

  handleChange = (e) => {
    let formManager = new FormManager(this.props.form);
    let target = e.target;
    let fieldName = e.target.name;

    formManager.changeFieldValue(fieldName, target.value)
        .validateField(fieldName)
        .validateForm();

    this.setState({form: formManager.getForm()});
  };

  getChildrenElements = () => {
    let children = [];

    for (let [fieldName, field] of Object.entries(this.props.form.children)) {
      if (field.type === 'text' || field.type === 'password' || field.type === 'hidden') {
        children.push(
            <FormInput
                key={`form-input-${fieldName}`}
                onChange={this.handleChange}
                field={field}
                name={fieldName}
            />
        );
      }
    }

    return children;
  };

  render() {
    return (
        <Form method={this.props.form.method}
              saveText={this.props.form.saveText}
              form={this.props.form}
              onSave={this.props.onSave}
              onCancel={this.props.onCancel}
              isLoading={this.props.isLoading}
        >
          <FormRow>
            <FormGroup sizeClass="col-12">
              {this.getChildrenElements()}
            </FormGroup>
          </FormRow>
          {this.props.children}
        </Form>
    );
  }
}

export default FormBuilder;