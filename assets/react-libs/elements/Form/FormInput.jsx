import React from "react";

class FormInput extends React.Component {
    render() {
        const field = this.props.field;
        const type = field.type;

        let validClasses = '';
        if (field.touched && field.isValid) {
            validClasses = 'is-valid';
        }

        if (field.touched && !field.isValid) {
            validClasses = 'is-invalid';
        }

        const classes = `form-control ${validClasses}`;

        return (field.isValid ?
                <>
                    <strong className="text-muted d-block mb-2 mt-10">{field.label}</strong>
                    <input autoComplete="off" name={this.props.name} onChange={this.props.onChange} type={type} className={classes}
                           placeholder={field.label} value={field.value} />
                </> :
                <>
                    <strong className="text-muted d-block mb-2 mt-10">{field.label}</strong>
                    <input autoComplete="off" name={this.props.name} onChange={this.props.onChange} type={type} className={classes}
                           placeholder={field.label} value={field.value} />
                        <div className="invalid-feedback">{field.invalidMessage}</div>
                </>
        )
    }
}

export default FormInput;