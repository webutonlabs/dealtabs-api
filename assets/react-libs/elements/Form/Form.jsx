import React from 'react';
import {MdCreate} from 'react-icons/md';

class Form extends React.Component {
  render() {
    return (
        <>
          <form method={this.props.method} onSubmit={this.props.onSave} className={'text-center'}>
            {this.props.children}
            <button disabled={!this.props.form.isValid || this.props.isLoading} type="submit" className="btn btn-primary mt-10">
              <span className="text-light">
                <MdCreate/>
              </span> {this.props.saveText}
            </button>
          </form>
        </>
    );
  }
}

export default Form;