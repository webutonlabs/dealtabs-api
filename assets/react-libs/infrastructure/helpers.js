const getCookie = (name) => {
    const cookies = `; ${document.cookie}`;
    const parts = cookies.split(`; ${name}=`);

    return parts.length === 2 ? parts.pop().split(';').shift() : null;
}

export {
    getCookie
}