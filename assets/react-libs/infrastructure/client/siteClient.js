import axios from 'axios'

const siteClient = () => {
    return axios.create({
        baseURL: `/${LOCALE}`,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });
}

export default siteClient;