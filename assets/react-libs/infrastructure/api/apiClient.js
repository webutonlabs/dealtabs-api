import axios from 'axios';
import {getCookie} from "../helpers";

export default axios.create({
    baseURL: API_HOST,
    headers: {
        'Content-Type': 'application/json',
        'X-AUTH-TOKEN': getCookie('token'),
        'X-DEVICE-ID': getCookie('device-id'),
        Accept: 'application/json',
    },
});