import axios from 'axios'

export default axios.create({
    baseURL: `${API_HOST}/auth`,
    headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json'
    }
});