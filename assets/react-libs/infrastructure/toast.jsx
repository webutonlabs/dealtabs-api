import {toast} from 'react-toastify';
import {MdError, FaThumbsUp} from 'react-icons/all';
import React from 'react';

const errorToast = (content, autoClose = 5000) => {
    const options = {
        position: 'bottom-right',
        autoClose: autoClose,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <MdError/>
            <span style={{'margin-left': '5px', 'font-size': '1.1rem'}}>{content}</span>
        </>
    );

    toast.error(body, options);
};

const successToast = (content, autoClose = 5000) => {
    const options = {
        position: 'bottom-right',
        autoClose: autoClose,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    };

    const body = (
        <>
            <FaThumbsUp/>
            <span style={{'margin-left': '5px', 'font-size': '0.75rem'}}>{content}</span>
        </>
    );

    toast.success(body, options);
};

const toastFromQuery = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);

    if (urlSearchParams.has('toast') && urlSearchParams.get('toast') === 'success') {
        successToast(urlSearchParams.get('message'));
    }

    if (urlSearchParams.has('toast') && urlSearchParams.get('toast') === 'error') {
        errorToast(urlSearchParams.get('message'));
    }
}

export {
    errorToast,
    successToast,
    toastFromQuery
};