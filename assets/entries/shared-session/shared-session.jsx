import './shared-session.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import SaveSharedSession from './components/SaveSharedSession';
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
    <SaveSharedSession/>,
    document.getElementById('save-shared-session'),
);