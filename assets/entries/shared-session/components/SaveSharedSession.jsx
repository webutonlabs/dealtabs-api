import React from 'react';
import {FaPlus, FaCheck} from 'react-icons/all';
import {ToastContainer} from "react-toastify";
import apiClient from "../../../react-libs/infrastructure/api/apiClient";
import {errorToast, successToast} from "../../../react-libs/infrastructure/toast";

export default class SaveSharedSession extends React.Component {
    constructor(props) {
        super(props);

        this.state = {saved: false};
    }

    save = () => {
        apiClient.post(`/sessions/shared/${SHARE_REFERENCE_ID}/save`).then(() => {
            this.setState({saved: true});

            successToast(TRANSLATION.success);
        }).catch(err => {
            errorToast(TRANSLATION[err.response.data.message]);
        });
    }

    render() {
        return (
            <>
                <div className="add-button">
                    <button className={`btn btn-lg ${this.state.saved ? 'btn-success' : 'btn-primary'}`} onClick={this.save} disabled={this.state.saved}>
                        {!this.state.saved && <FaPlus />}
                        {this.state.saved && <FaCheck />}
                        <span style={{'margin-left': '5px'}}>{this.state.saved ? TRANSLATION.saved : TRANSLATION.save_to_account}</span>
                    </button>
                </div>
                <ToastContainer />
            </>
        )
    }
}