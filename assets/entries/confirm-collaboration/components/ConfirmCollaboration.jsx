import React from "react";
import {ToastContainer} from "react-toastify";
import apiClient from "../../../react-libs/infrastructure/api/apiClient";

class ConfirmCollaboration extends React.Component {
    componentDidMount() {
        apiClient.post('/collaboration/confirm-invitation', {session: SESSION_ID}).then(() => {
            //
        }).catch(err => {
            console.log(err);
        })
    }

    render() {
        return (
            <>
                <ToastContainer />
            </>
        )
    }
}

export default ConfirmCollaboration;