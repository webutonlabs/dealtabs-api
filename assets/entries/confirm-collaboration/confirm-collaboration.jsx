import 'react-toastify/dist/ReactToastify.css';
import React from 'react';
import ReactDOM from 'react-dom';
import ConfirmCollaboration from "./components/ConfirmCollaboration";

ReactDOM.render(
    <ConfirmCollaboration/>,
    document.getElementById('confirm-body'),
);