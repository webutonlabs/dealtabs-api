import './password-recovery.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import 'react-toastify/dist/ReactToastify.css';
import PasswordRecoveryCard from "./components/PasswordRecoveryCard";

ReactDOM.render(
    <PasswordRecoveryCard/>,
    document.getElementById('password-recovery-body'),
);