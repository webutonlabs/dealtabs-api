import React from 'react';
import FormBuilder from '../../../react-libs/components/FormBuilder/FormBuilder';
import {Email} from '../../../react-libs/components/FormBuilder/validators';
import {
    errorToast,
    successToast,
} from '../../../react-libs/infrastructure/toast';
import FormManager
    from '../../../react-libs/components/FormBuilder/FormManager';
import authClient from '../../../react-libs/infrastructure/api/authClient';

export class RequestForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                method: 'POST',
                isValid: false,
                children: {
                    email: {
                        label: TRANSLATION.email_input_label,
                        type: 'text',
                        value: '',
                        validators: [
                            new Email(TRANSLATION.invalid_email_message)
                        ]
                    }
                },
                saveText: TRANSLATION.send_request
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();
        this.setState({isLoading: true});

        authClient.post('/password-recovery', {email: this.state.form.children.email.value}).then(() => {
            successToast(TRANSLATION.code_is_sent_to_email);

            this.props.next();
            this.setState({isLoading: false});
        }).catch((error) => {
            const formManager = new FormManager(this.state.form);

            error.response.data.errors.forEach(error => {
                if (error.pointer === 'email') {
                    formManager.makeFieldInvalid('email', TRANSLATION.user_not_found);
                    errorToast(TRANSLATION.user_not_found);

                    this.setState({form: formManager.validateForm().getForm()});
                }
            });

            this.setState({isLoading: false});
        })
    }

    render() {
        return (
            <FormBuilder isLoading={this.state.isLoading} form={this.state.form} onSave={this.onSave} />
        )
    }
}