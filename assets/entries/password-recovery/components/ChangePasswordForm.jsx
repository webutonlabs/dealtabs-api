import React from 'react';
import FormBuilder from '../../../react-libs/components/FormBuilder/FormBuilder';
import {Length} from '../../../react-libs/components/FormBuilder/validators';
import {
    errorToast,
    successToast,
} from '../../../react-libs/infrastructure/toast';
import authClient from '../../../react-libs/infrastructure/api/authClient';
import FormManager
    from '../../../react-libs/components/FormBuilder/FormManager';

export class ChangePasswordForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                method: 'POST',
                isValid: false,
                children: {
                    code: {
                        label: TRANSLATION.code_label,
                        type: 'text',
                        value: '',
                        validators: [
                            new Length(8, TRANSLATION.invalid_code_length_message)
                        ]
                    },
                    password: {
                        label: TRANSLATION.password_input_label,
                        type: 'password',
                        value: '',
                        validators: [
                            new Length(5, TRANSLATION.invalid_password_length_message)
                        ]
                    }
                },
                saveText: TRANSLATION.reset_password
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();
        this.setState({isLoading: true});

        authClient.post('/password-recovery/change-password', {
            password: this.state.form.children.password.value,
            code: this.state.form.children.code.value
        }).then(() => {
            successToast(TRANSLATION.success);
            setTimeout(() => {
                window.location.href = LOGIN_URL;
            }, 1000);
        }).catch((error) => {
            const formManager = new FormManager(this.state.form);

            error.response.data.errors.forEach(error => {
                if (error.pointer === 'code') {
                    formManager.makeFieldInvalid('code', TRANSLATION.code_not_found);
                    errorToast(TRANSLATION.code_not_found);

                    this.setState({form: formManager.validateForm().getForm()});
                }
            });

            this.setState({isLoading: false});
        })
    }

    render() {
        return (
            <FormBuilder isLoading={this.state.isLoading} form={this.state.form} onSave={this.onSave} />
        )
    }
}