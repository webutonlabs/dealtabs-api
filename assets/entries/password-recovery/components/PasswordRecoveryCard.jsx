import React from 'react';
import {RequestForm} from "./RequestForm";
import {ChangePasswordForm} from "./ChangePasswordForm";
import {ToastContainer} from "react-toastify";

export default class PasswordRecoveryCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            step: 1
        }
    }

    next = () => {
        this.setState({step: 2});
    }

    render() {
        return (
            <>
                <div className="card-header text-center">{TRANSLATION.title}</div>
                <div className="card-body">
                    {this.state.step === 1 && <RequestForm next={this.next} />}
                    {this.state.step === 2 && <ChangePasswordForm />}
                </div>
                <ToastContainer/>
            </>
        );
    }
}