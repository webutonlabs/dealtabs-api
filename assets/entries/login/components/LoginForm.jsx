import React from 'react';
import FormBuilder from '../../../react-libs/components/FormBuilder/FormBuilder';
import {
    Email,
    Length,
} from '../../../react-libs/components/FormBuilder/validators';

export class LoginForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                method: 'POST',
                isValid: false,
                children: {
                    login: {
                        label: TRANSLATION.login_input_label,
                        type: 'text',
                        value: LAST_LOGIN,
                        validators: [
                            new Email(TRANSLATION.invalid_email_message)
                        ]
                    },
                    password: {
                        label: TRANSLATION.password_input_label,
                        type: 'password',
                        value: '',
                        validators: [
                            new Length(5, TRANSLATION.invalid_password_length_message)
                        ]
                    }
                },
                saveText: TRANSLATION.form_save_text
            }
        }
    }

    onSave = (e) => {
        this.setState({isLoading: true});
    }

    render() {
        return (
            <FormBuilder isLoading={this.state.isLoading} form={this.state.form} onSave={this.onSave}>
                <input style={{display: 'none'}} type={'checkbox'} name={'_remember_me'} checked={true}/>
                <input type={'hidden'} name={'_csrf_token'} value={CSRF_TOKEN}/>
            </FormBuilder>
        )
    }
}