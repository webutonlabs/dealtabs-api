import React from 'react';
import {FaGooglePlusG, FaFacebook, RiLockPasswordFill, FaUserCircle} from 'react-icons/all';
import {LoginForm} from './LoginForm';
import {ToastContainer} from "react-toastify";
import {toastFromQuery} from "../../../react-libs/infrastructure/toast";

export default class LoginCard extends React.Component {
    componentDidMount() {
        toastFromQuery();
    }

    render() {
        return (
            <>
                <div className="card-header text-center">{TRANSLATION.title}</div>
                <div className="card-body">
                    <LoginForm/>
                </div>
                <div className="card-footer">
                    <div className="container">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-12 text-center footer-buttons">
                                <button onClick={() => {window.location.href = GOOGLE_AUTH_URL}} className="btn btn-light"><FaGooglePlusG/> {TRANSLATION.google}</button>
                                <button style={{display: 'none'}} id={'facebook-button'} className="btn btn-light"><FaFacebook/> {TRANSLATION.facebook}</button>
                                <button onClick={() => {window.location.href = REGISTRATION_URL}} className="btn btn-light">
                                    <FaUserCircle/> {TRANSLATION.sign_up}
                                </button>
                                <button onClick={() => {window.location.href = PASSWORD_RECOVERY_URL}} className="btn btn-light"><RiLockPasswordFill/> {TRANSLATION.forgot_password}</button>
                            </div>
                        </div>
                    </div>
                    <div id={'facebook-frame'} className="row justify-content-center align-items-center" style={{display: 'none'}}>
                        <div className="col-12 text-center">
                            <div data-onlogin="onFacebookLogin()" className="fb-login-button mt-1" data-width="" data-size="medium"
                                 data-button-type="continue_with" data-layout="default" data-auto-logout-link="false"
                                 data-use-continue-as="false"/>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </>
        );
    }
}