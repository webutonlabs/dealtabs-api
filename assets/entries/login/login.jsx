import './login.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import LoginCard from './components/LoginCard';
import 'react-toastify/dist/ReactToastify.css';

window.fbAsyncInit = function() {
    FB.init({
        appId      : FACEBOOK_APP_ID,
        cookie     : true,
        xfbml      : true,
        version    : 'v9.0'
    });

    FB.AppEvents.logPageView();
    FB.getLoginStatus(function(response) {
        const facebookButton = document.getElementById('facebook-button');
        const facebookFrame = document.getElementById('facebook-frame');

        if (response.status === 'connected') {
            const accessToken = response.authResponse.accessToken;

            facebookButton.style.display = 'inline-block';
            facebookButton.setAttribute('data-token', accessToken);

            facebookButton.addEventListener('click', function (e) {
                e.target.setAttribute('disabled', 'true');

                const xhr = new XMLHttpRequest();
                xhr.open('POST', `${API_HOST}/auth`, true);
                xhr.responseType = 'json';
                xhr.setRequestHeader("Content-Type", "application/json");
                xhr.setRequestHeader("Accept", "application/json");
                xhr.onload = () => {
                    window.location.href = `/${LOCALE}/user?access_token=${xhr.response.access_token}&expires_at=${xhr.response.expires_at}`
                }

                const data = {
                    grant_type: 'facebook_access_token',
                    credentials: {
                        access_token: e.target.getAttribute('data-token')
                    },
                    context: {
                        locale: LOCALE
                    }
                }

                xhr.send(JSON.stringify(data));
            });
        } else {
            facebookFrame.style.display = 'flex';
        }
    });
};

ReactDOM.render(
    <LoginCard/>,
    document.getElementById('authentication-body'),
);