import './registration.scss';

import React from 'react';
import ReactDOM from 'react-dom';
import RegistrationCard from "./components/RegistrationCard";
import 'react-toastify/dist/ReactToastify.css';

ReactDOM.render(
    <RegistrationCard/>,
    document.getElementById('registration-body'),
);