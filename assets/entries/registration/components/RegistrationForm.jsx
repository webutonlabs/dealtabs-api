import React from 'react';
import FormBuilder from '../../../react-libs/components/FormBuilder/FormBuilder';
import {
    Email,
    Length, NotBlank,
} from '../../../react-libs/components/FormBuilder/validators';

import authClient from "../../../react-libs/infrastructure/api/authClient";
import {ToastContainer} from "react-toastify";
import {errorToast} from "../../../react-libs/infrastructure/toast";
import FormManager from "../../../react-libs/components/FormBuilder/FormManager";

export class RegistrationForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            form: {
                method: 'POST',
                isValid: false,
                children: {
                    email: {
                        label: TRANSLATION.email_input_label,
                        type: 'text',
                        value: '',
                        validators: [
                            new Email(TRANSLATION.invalid_email_message)
                        ]
                    },
                    name: {
                        label: TRANSLATION.name_input_label,
                        type: 'text',
                        value: '',
                        validators: [
                            new NotBlank(TRANSLATION.invalid_name_message)
                        ]
                    },
                    surname: {
                        label: TRANSLATION.surname_input_label,
                        type: 'text',
                        value: '',
                    },
                    password: {
                        label: TRANSLATION.password_input_label,
                        type: 'password',
                        value: '',
                        validators: [
                            new Length(5, TRANSLATION.invalid_password_length_message)
                        ]
                    }
                },
                saveText: TRANSLATION.form_save_text
            }
        }
    }

    onSave = (e) => {
        e.preventDefault();
        this.setState({isLoading: true});

        let data = {
            name: this.state.form.children.name.value,
            email: this.state.form.children.email.value,
            password: this.state.form.children.password.value,
            locale: LOCALE
        };

        if (this.state.form.children.surname.value.length > 0) {
            data.surname = this.state.form.children.surname.value;
        }

        authClient.post('/registration/by-username', data).then(() => {
            this.props.setEmail(data.email);
            this.props.nextStep();
        }).catch((error) => {
            const formManager = new FormManager(this.state.form);

            error.response.data.errors.forEach(error => {
                if (error.pointer === 'email') {
                    formManager.makeFieldInvalid('email', TRANSLATION.user_exists_msg);
                    errorToast(TRANSLATION.user_exists_msg);
                }
            });

            this.setState({isLoading: false, form: formManager.validateForm().getForm()});
        })

    }

    render() {
        return (
            <FormBuilder isLoading={this.state.isLoading} form={this.state.form} onSave={this.onSave}>
                <input style={{display: 'none'}} id={'disable-pwd-mgr-1'} type={'password'} name={'disable-pwd-mgr-1'}
                       value={'disable-pwd-mgr-1'}/>
                <input style={{display: 'none'}} id={'disable-pwd-mgr-2'} type={'password'} name={'disable-pwd-mgr-2'}
                       value={'disable-pwd-mgr-2'}/>
                <input style={{display: 'none'}} id={'disable-pwd-mgr-3'} type={'password'} name={'disable-pwd-mgr-3'}
                       value={'disable-pwd-mgr-3'}/>
                <ToastContainer/>
            </FormBuilder>
        )
    }
}