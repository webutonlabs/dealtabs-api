import React from 'react';
import {successToast} from "../../../react-libs/infrastructure/toast";
import authClient from "../../../react-libs/infrastructure/api/authClient";

class RegistrationResendEmailCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            seconds: 30,
            intervalId: null
        }
    }

    componentDidMount() {
        successToast(TRANSLATION.email_confirmation_message);
        this.setTicker();
    }

    setTicker = () => {
        this.setState({seconds: 30});

        const intervalId = setInterval(() => {
            this.setState({seconds: --this.state.seconds})
            if (this.state.seconds === 0) {
                clearInterval(this.state.intervalId);
            }
        }, 1000);

        this.setState({intervalId: intervalId});
    }

    resendEmail = () => {
        authClient.post('/registration/resend-email', {email: this.props.getEmail()}).then(() => {
            successToast(TRANSLATION.email_is_resent);
            this.setTicker();
        })
    }

    render() {
        return (
            <div className={'row'}>
                <div className={'col-12 text-center'}>
                    <h3>{TRANSLATION.email_is_sent_to} <span className={'title'}>{this.props.getEmail()}</span></h3>
                    <h6>{TRANSLATION.did_not_receive_email}</h6>
                    <div>
                        <button onClick={this.resendEmail} disabled={this.state.seconds !== 0} className={'btn btn-primary'}>
                            {this.state.seconds !== 0 &&
                                <span>{TRANSLATION.resend_in} {this.state.seconds} {TRANSLATION.seconds}</span>
                            }
                            {this.state.seconds === 0 &&
                                <span>{TRANSLATION.resend_email}</span>
                            }
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default RegistrationResendEmailCard;