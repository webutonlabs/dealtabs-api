import React from 'react';
import {FaGooglePlusG, FaFacebook, FaUser} from 'react-icons/all';
import {RegistrationForm} from "./RegistrationForm";
import RegistrationResendEmailCard from "./RegistrationResendEmailCard";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export default class RegistrationCard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            step: 1,
            email: ''
        }
    }

    nextStep = () => {
        this.setState({step: ++this.state.step})
    }

    setEmail = (email) => {
        this.setState({email: email});
    }

    getEmail = (email) => {
        return this.state.email;
    }

    render() {
        return (
            <>
                <div className="card-header text-center">{TRANSLATION.title}</div>
                <div className="card-body">
                    {this.state.step === 1 &&
                        <RegistrationForm nextStep={this.nextStep} setEmail={this.setEmail}/>
                    }
                    {this.state.step === 2 &&
                        <RegistrationResendEmailCard getEmail={this.getEmail}/>
                    }
                </div>
                <div className="card-footer">
                    <div className="container">
                        <div className="row justify-content-center align-items-center">
                            <div className="col-12 text-center footer-buttons">
                                <button onClick={() => {
                                    window.location.href = LOGIN_URL
                                }} className="btn btn-light"><FaUser/> {TRANSLATION.sign_in}</button>
                            </div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </>
        );
    }
}