import './index.scss';
import 'bootstrap/js/src/carousel';

import React from 'react';
import ReactDOM from 'react-dom';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainerResolver} from "./components/ToastContainerResolver";
import ExtensionReviewsComponent from "./components/ExtensionReviews/ExtensionReviewsComponent";

ReactDOM.render(
    <ToastContainerResolver />,
    document.getElementById('toast-container'),
);

ReactDOM.render(
    <ExtensionReviewsComponent />,
    document.getElementById('reviews'),
);
