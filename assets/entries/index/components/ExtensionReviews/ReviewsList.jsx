import React from 'react';
import Review from "./Review";

class ReviewsList extends React.Component {
    render() {
        return (
            <>
                <div className={'row'}>
                    {
                        this.props.reviews.map(review => {
                            return <Review key={review.id} review={review} />
                        })
                    }
                </div>
            </>
        )
    }
}

export default ReviewsList;