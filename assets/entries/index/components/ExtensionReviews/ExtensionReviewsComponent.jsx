import React from 'react';
import siteClient from "../../../../react-libs/infrastructure/client/siteClient";
import ReviewsList from "./ReviewsList";
import CreateReview from "./CreateReview";

class ExtensionReviewsComponent extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            reviews: [],
            page: 1,
            openCreateReview: false
        }
    }

    componentDidMount() {
        this.fetchReviews();
    }

    fetchReviews = () => {
        siteClient().get(`/reviews?page=${this.state.page}&limit=3`).then(response => {
            this.setState({reviews: [...this.state.reviews, ...response.data]});
        });
    }

    loadMore = () => {
        this.state.page = ++this.state.page;
        this.fetchReviews();
    }

    setReviews = (reviews) => {
        this.setState({reviews: reviews});
    }

    render() {
        return (
            <>
                <div className={'row'}>
                    <div className={'col-12'}>
                        <ReviewsList reviews={this.state.reviews}/>
                    </div>
                    <div className={'col-12'}>
                        <div className={'row mt-3 justify-content-center'}>
                            <div className={'col-12 text-center'}>
                                <button onClick={this.loadMore} className={'btn btn-primary'}>{LOAD_MORE}</button>
                                <button className={'btn btn-primary ml-1'} onClick={() => {this.setState({openCreateReview: !this.state.openCreateReview})}}>{LEAVE_FEEDBACK}</button>
                            </div>
                            {this.state.openCreateReview &&
                                <div className={'col-12 col-md-6 mt-3'}>
                                    <CreateReview reviews={this.state.reviews} setReviews={this.setReviews} />
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default ExtensionReviewsComponent;