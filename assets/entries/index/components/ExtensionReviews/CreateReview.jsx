import React from 'react';
import ReactStars from "react-rating-stars-component/dist/react-stars";
import {FaStar, IoMdStarOutline} from "react-icons/all";
import siteClient from "../../../../react-libs/infrastructure/client/siteClient";
import {toast} from "react-toastify";
import {errorToast} from "../../../../react-libs/infrastructure/toast";

class CreateReview extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            rating: 4,
            message: ''
        }
    }

    handleNameChange = (event) => {
        this.setState({
            name: event.target.value
        });
    }

    handleMessageChange = (event) => {
        this.setState({
            message: event.target.value
        });
    }

    ratingChanged = (newRating) => {
        this.setState({rating: newRating});
    }

    publishFeedback = () => {
        if (this.state.name === '') {
            errorToast(NAME_CANNOT_BE_EMPTY, 2000);
            return;
        }

        if (this.state.comment === '') {
            errorToast(COMMENT_CANNOT_BE_EMPTY, 2000);
            return;
        }

        siteClient().post("/reviews", {
            name: this.state.name,
            message: this.state.message,
            stars: this.state.rating
        }).then(res => {
            toast.info(FEEDBACK_CREATED, {
                position: 'bottom-right',
                autoClose: 1500,
                hideProgressBar: false,
                closeOnClick: true,
                progress: undefined
            });

            this.props.setReviews([res.data, ...this.props.reviews]);
            document.getElementById('reviews-section').scrollIntoView({behavior: "smooth"});
        })
    }

    render() {
        return (
            <>
                <div className={'card shadow'}>
                    <div className={'card-body'}>
                        <div className={'row'}>
                            <div className={'col-12'}>
                                <label>{YOUR_NAME}</label>
                                <input onChange={this.handleNameChange} className={'form-control'} type={'text'}/>
                            </div>
                            <div className={'col-12 mt-2'}>
                                <div>{YOUR_RATING}</div>
                                <ReactStars
                                    onChange={this.ratingChanged}
                                    count={5}
                                    size={15}
                                    value={this.state.rating}
                                    activeColor="#FEB954"
                                    color="#FEB954"
                                    emptyIcon={<IoMdStarOutline /> }
                                    filledIcon={<FaStar />}
                                    isHalf={false}
                                />
                            </div>
                            <div className={'col-12 mt-2'}>
                                <div>{YOUR_MESSAGE}</div>
                                <textarea onChange={this.handleMessageChange} className={'form-control'}/>
                            </div>
                            <div className={'col-12 mt-2 text-right'}>
                                <button onClick={this.publishFeedback} className={'btn btn-primary'}>{LEAVE_FEEDBACK}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default CreateReview;