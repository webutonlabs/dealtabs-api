import React from 'react';
import ReactStars from "react-rating-stars-component/dist/react-stars";
import {FaStar, IoMdStarOutline} from "react-icons/all";

class Review extends React.Component {
    render() {
        return (
            <>
                <div className={'col-12 mt-1'}>
                    <div className={'card shadow'}>
                        <div className={'card-body'}>
                            <h6 style={{marginBottom: 0}}>{this.props.review.name}</h6>
                            <ReactStars
                                count={5}
                                size={15}
                                value={this.props.review['stars']}
                                activeColor="#FEB954"
                                color="#FEB954"
                                emptyIcon={<IoMdStarOutline /> }
                                filledIcon={<FaStar />}
                                edit={false}
                                isHalf={false}
                            />
                            <p className={'mt-1'}>{this.props.review.message}</p>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Review;