import React from 'react';
import {toastFromQuery} from "../../../react-libs/infrastructure/toast";
import {ToastContainer} from "react-toastify";

export class ToastContainerResolver extends React.Component {
    componentDidMount() {
        toastFromQuery();
    }

    render() {
        return <ToastContainer />
    }
}