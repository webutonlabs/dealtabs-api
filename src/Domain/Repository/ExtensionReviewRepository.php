<?php

namespace App\Domain\Repository;

use App\Domain\Entity\ExtensionReview;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

class ExtensionReviewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ExtensionReview::class);
    }

    public function getList(): Query
    {
        $qb = $this->createQueryBuilder('r');
        $qb->orderBy('r.id', 'DESC');

        return $qb->getQuery();
    }
}