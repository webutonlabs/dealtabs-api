<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @ORM\Entity()
 */
class DeletionFeedback extends AbstractEntity
{
    public const REASON_SLOW = 'slow';
    public const REASON_UNUSEFUL = 'unuseful';
    public const REASON_OTHER = 'other';

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $reason;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $browser;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private ?string $message = null;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private \DateTimeInterface $creationDate;

    public function __construct(string $browser)
    {
        $this->browser = $browser;
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getBrowser(): string
    {
        return $this->browser;
    }

    public function getReason(): string
    {
        return $this->reason;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    public function setReason(string $reason): void
    {
        $this->reason = $reason;
    }
}