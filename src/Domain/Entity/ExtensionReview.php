<?php

namespace App\Domain\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 */
class ExtensionReview extends AbstractEntity
{
    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private string $message;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $stars;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=false)
     */
    private \DateTimeInterface $creationDate;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $locale;

    public function __construct(string $locale)
    {
        $this->locale = $locale;
        $this->stars = 0;
        $this->creationDate = new \DateTimeImmutable();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    public function getStars(): int
    {
        return $this->stars;
    }

    public function setStars(int $stars): void
    {
        $this->stars = $stars;
    }

    public function getCreationDate(): \DateTimeImmutable|\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeImmutable|\DateTimeInterface $creationDate): void
    {
        $this->creationDate = $creationDate;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }
}