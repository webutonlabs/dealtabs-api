<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/", name="site")
 */
class IndexController extends AbstractController
{
    public function __invoke(Request $request)
    {
        return $this->redirectToRoute('site_index', ['_locale' => $request->getLocale()]);
    }
}