<?php

namespace App\Controller\Site;

use App\Domain\Entity\DeletionFeedback;
use App\Forms\DeletionFeedbackType;
use Sinergi\BrowserDetector\Browser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/deletion-feedback", name="deletion_feedback_")
 */
class DeletionFeedbackController extends AbstractController
{
    /**
     * @Route("", name="start")
     */
    public function start(Request $request)
    {
        $browser = new Browser();

        $form = $this->createForm(DeletionFeedbackType::class, new DeletionFeedback($browser->getName()));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->persist($form->getData());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('deletion_feedback_finish');
        }

        return $this->render('site/pages/deletion_feedback/deletion_feedback.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/thank-you", name="finish")
     */
    public function finish()
    {
        return $this->render('site/pages/deletion_feedback/feedback_left.html.twig');
    }
}