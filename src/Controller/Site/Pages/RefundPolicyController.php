<?php

namespace App\Controller\Site\Pages;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/refund-policy", name="site_refund_policy")
 */
class RefundPolicyController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/refund_policy.html.twig');
    }
}