<?php

namespace App\Controller\Site\Pages;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/terms-and-conditions", name="site_terms_and_conditions")
 */
class TermsAndConditionsController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/terms_and_conditions.html.twig');
    }
}