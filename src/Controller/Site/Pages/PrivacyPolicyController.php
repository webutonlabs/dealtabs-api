<?php

namespace App\Controller\Site\Pages;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/privacy-policy", name="site_privacy_policy")
 */
class PrivacyPolicyController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/privacy_policy.html.twig');
    }
}