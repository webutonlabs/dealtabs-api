<?php

namespace App\Controller\Site\Faq;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/delete-account", name="how_to_delete_account")
 */
class HowToDeleteAccountController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/how_to_delete_account.html.twig');
    }
}