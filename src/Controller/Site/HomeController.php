<?php

namespace App\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/", name="site_index")
 */
class HomeController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/index.html.twig');
    }
}