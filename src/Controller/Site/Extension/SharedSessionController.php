<?php

namespace App\Controller\Site\Extension;

use App\Infrastructure\ExternalService\Api\ApiClient;
use App\Infrastructure\Security\Provider\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/shared-session/{shareReference}", name="site_shared_session_")
 */
class SharedSessionController extends AbstractController
{
    /**
     * @Route("", name="index", methods={"GET"})
     */
    public function index(Request $request, string $shareReference, TranslatorInterface $translator)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof User) {
            return $this->redirectToRoute('user_login', [
                'toast' => 'error',
                'message' => $translator->trans('You have to login')
            ]);
        }

        $client = new ApiClient($user->getApiCredentials()->getAccessToken(), $request->headers->get('device-id'));

        try {
            $session = $client->getSessionBySharedReference($shareReference);
        } catch (\Exception $e) {
            return $this->redirectToRoute('site_index', [
                'toast' => 'error',
                'message' => $translator->trans('Session was not found')
            ]);
        }

        return $this->render('site/pages/shared_session.html.twig',
                             ['session' => $session, 'shareReference' => $shareReference]);
    }
}