<?php

namespace App\Controller\Site\Extension;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/collaborated-session", name="site_confirm_collaboration_")
 */
class ConfirmCollaborationController extends AbstractController
{
    /**
     * @Route("/{sessionId}", name="by_session_id")
     */
    public function confirmById(string $sessionId)
    {
        if (!$this->getUser()) {
            return $this->redirectToRoute('user_login');
        }

        return $this->render('site/pages/confirm_collaboration.html.twig', ['sessionId' => $sessionId]);
    }
}