<?php

namespace App\Controller\Site\Extension;

use App\Domain\Entity\ExtensionReview;
use App\Domain\Repository\ExtensionReviewRepository;
use App\Infrastructure\Service\Paginator;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/reviews", name="extension_reviews_")
 */
class ReviewController extends AbstractController
{
    /**
     * @Route("", name="list", methods={"GET"})
     */
    public function listReviews(Paginator $paginator, ExtensionReviewRepository $extensionReviewRepository)
    {
        return $this->json($paginator->paginate($extensionReviewRepository->getList()));
    }

    /**
     * @Route("", name="create", methods={"POST"})
     */
    public function createReview(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $review = new ExtensionReview($request->getLocale());
        $review->setName($data['name']);
        $review->setStars($data['stars']);
        $review->setMessage($data['message']);

        $this->getDoctrine()->getManager()->persist($review);
        $this->getDoctrine()->getManager()->flush();

        return $this->json($review);
    }
}