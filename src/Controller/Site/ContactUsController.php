<?php

namespace App\Controller\Site;

use App\Domain\Entity\SentMessage;
use App\Forms\SendMessageType;
use App\Infrastructure\Mailer\Emails\SentMessageEmail;
use App\Infrastructure\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/contact-us", name="site_contact_us_")
 */
class ContactUsController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request, Mailer $mailer)
    {
        $form = $this->createForm(SendMessageType::class, new SentMessage());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var SentMessage $sentMessage */
            $sentMessage = $form->getData();

            $this->getDoctrine()->getManager()->persist($sentMessage);
            $this->getDoctrine()->getManager()->flush();

            try {
                $email = new SentMessageEmail($sentMessage->getName(), $sentMessage->getEmail(), $sentMessage->getMessage());
                $mailer->send($email->getEnvelope());
            } catch (\Exception $e) {
                dd($e);
            }

            return $this->redirectToRoute('site_contact_us_index', ['success' => 'true']);
        }

        return $this->render('site/pages/contaсt_us.html.twig', ['form' => $form->createView()]);
    }
}