<?php

namespace App\Controller\Site;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/error", name="site_error")
 */
class ErrorController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('site/pages/error.html.twig');
    }
}