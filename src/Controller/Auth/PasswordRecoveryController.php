<?php

namespace App\Controller\Auth;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/password-recovery", name="site_password_recovery_")
 */
class PasswordRecoveryController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function __invoke()
    {
        return $this->render('site/auth/password_recovery.html.twig');
    }
}