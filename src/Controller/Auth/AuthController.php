<?php

namespace App\Controller\Auth;

use App\Infrastructure\Security\GoogleAuthManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class AuthController extends AbstractController
{
    /**
     * @Route("/{_locale}/user", name="user_login")
     */
    public function loginPage(AuthenticationUtils $authenticationUtils, UrlGeneratorInterface $urlGenerator): Response {
        $error = $authenticationUtils->getLastAuthenticationError();
        if ($error) {
            $error = $error->getMessage();
        }

        return $this->render(
            'site/auth/login.html.twig',
            [
                'error' => $error,
                'lastLogin' => $authenticationUtils->getLastUsername(),
                'googleAuthUrl' => $urlGenerator->generate('google_auth_start')
            ]
        );
    }

    /**
     * @Route("/{_locale}/login-succeeded", name="user_login_succeeded")
     */
    public function loginSucceeded()
    {
        return $this->render('site/auth/login_succeeded.html.twig');
    }

    /**
     * @Route("/logout", name="user_logout")
     */
    public function logout()
    {
        return $this->redirectToRoute('user_login');
    }
}