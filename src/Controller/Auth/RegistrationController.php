<?php

namespace App\Controller\Auth;

use App\Infrastructure\ExternalService\Api\ApiAuthClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 *
 * @Route("/{_locale}/registration", name="site_registration_")
 */
class RegistrationController extends AbstractController
{
    /**
     * @Route("", name="index")
     */
    public function index(Request $request)
    {
        if ($request->query->has('collaborated_session')) {
            $request->getSession()->set('collaborated_session', $request->query->get('collaborated_session'));
            $request->getSession()->set('collaborated_session_confirmed', false);
        }

        return $this->render('site/auth/registration.html.twig');
    }

    /**
     * @Route("/{token}/confirm-email", name="confirm_email")
     */
    public function confirmEmail(string $token, ApiAuthClient $apiClient, TranslatorInterface $translator)
    {
        $apiClient->confirmEmail($token);

        return $this->redirectToRoute('user_login', [
            'toast' => 'success',
            'message' => $translator->trans('Email is confirmed')
        ]);
    }
}