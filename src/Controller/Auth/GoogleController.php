<?php

namespace App\Controller\Auth;

use App\Infrastructure\ExternalService\Api\ApiAuthClient;
use App\Infrastructure\Security\GoogleAuthManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/{_locale}/google", name="google_")
 */
class GoogleController extends AbstractController
{
    /**
     * @Route("/start", name="auth_start")
     */
    public function start(GoogleAuthManager $googleAuthManager)
    {
        return $this->redirect($googleAuthManager->loginUrl());
    }

    /**
     * @Route("/auth-code", name="auth_code")
     */
    public function authCode(Request $request, ApiAuthClient $apiAuthClient)
    {
        $data = $apiAuthClient->authByGoogleCode($request->query->get('code'), ['locale' => $request->getLocale()]);

        return $this->redirectToRoute('user_login', [
            'access_token' => $data['access_token'],
            'expires_at' => $data['expires_at']
        ]);
    }
}