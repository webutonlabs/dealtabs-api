<?php

namespace App\Forms;

use App\Domain\Entity\SentMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SendMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $builder
            ->add(
                'name',
                TextType::class,
                [
                    'label' => 'Your name',
                    'label_attr' => [
                        'placeholder' => 'Your name'
                    ]
                ]
            )
            ->add(
                'email',
                EmailType::class,
                [
                    'label' => 'Your email',
                    'label_attr' => [
                        'placeholder' => 'Your email'
                    ]
                ]
            )
            ->add(
                'message',
                TextareaType::class,
                [
                    'label' => 'How can we help you?',
                    'label_attr' => [
                        'placeholder' => 'Your message'
                    ]
                ]
            );

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Send message',
                'attr' => [
                    'class' => 'btn-primary'
                ]
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => SentMessage::class
            ]
        );
    }
}