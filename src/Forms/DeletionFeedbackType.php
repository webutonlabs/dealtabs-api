<?php

namespace App\Forms;

use App\Domain\Entity\DeletionFeedback;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class DeletionFeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder = $builder
            ->add(
                'reason',
                ChoiceType::class,
                [
                    'label' => 'Reason',
                    'choices' => [
                        'Slow' => DeletionFeedback::REASON_SLOW,
                        'Unuseful' => DeletionFeedback::REASON_UNUSEFUL,
                        'Other' => DeletionFeedback::REASON_OTHER
                    ],
                    'expanded' => true
                ]
            )
            ->add(
                'message',
                TextareaType::class,
                [
                    'label' => 'Message',
                    'label_attr' => [
                        'class' => 'mt-3'
                    ]
                ]
            );

        return $builder->add(
            'save',
            SubmitType::class,
            [
                'label' => 'Leave feedback',
                'attr' => [
                    'class' => 'mt-3 btn-primary'
                ]
            ]
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        return $resolver->setDefaults(
            [
                'allow_extra_fields' => true,
                'data_class' => DeletionFeedback::class
            ]
        );
    }
}