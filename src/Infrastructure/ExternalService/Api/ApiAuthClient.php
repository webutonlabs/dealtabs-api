<?php

namespace App\Infrastructure\ExternalService\Api;

use Symfony\Component\HttpClient\HttpClient;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class ApiAuthClient
{
    private string $apiHost;

    public function __construct()
    {
        $this->apiHost = $_ENV['API_HOST'];
    }

    public function confirmEmail(string $token): void
    {
        HttpClient::create()->request(
            'POST',
            $this->apiHost . '/auth/registration/confirm-email',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode(['token' => $token])
            ]
        );
    }

    public function authByUsername(string $email, string $password)
    {
        $data = [
            'grant_type' => 'username_password',
            'credentials' => [
                'login' => $email,
                'password' => $password
            ]
        ];

        return HttpClient::create()->request(
            'POST',
            $this->apiHost . '/auth',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($data)
            ]
        )->toArray();
    }

    public function authByGoogleCode(string $code, array $context = [])
    {
        $data = [
            'grant_type' => 'google_code',
            'credentials' => [
                'code' => $code
            ],
            'context' => $context
        ];

        return HttpClient::create()->request(
            'POST',
            $this->apiHost . '/auth',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode($data)
            ]
        )->toArray();
    }

    public function resendConfirmationEmail(string $email)
    {
        return HttpClient::create()->request(
            'POST',
            $this->apiHost . '/auth/registration/resend-email',
            [
                'headers' => [
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode(['email' => $email])
            ]
        );
    }
}