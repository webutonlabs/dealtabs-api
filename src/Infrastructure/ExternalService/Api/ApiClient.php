<?php

namespace App\Infrastructure\ExternalService\Api;

use Symfony\Component\HttpClient\HttpClient;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class ApiClient
{
    private string $apiHost;
    private string $accessToken;
    private ?string $deviceId;

    public function __construct(string $accessToken, ?string $deviceId = null)
    {
        $this->apiHost = $_ENV['API_HOST'];
        $this->accessToken = $accessToken;
        $this->deviceId = $deviceId;
    }

    public function getSessionBySharedReference(string $shareReferenceId)
    {
        return $this->request('GET', "/sessions/shared/$shareReferenceId");
    }

    public function getUserData()
    {
        return $this->request('GET', '/user');
    }

    private function request(string $method, string $uri, array $data = [])
    {
        $options = [
            'headers' => [
                'X-AUTH-TOKEN' => $this->accessToken,
                'X-DEVICE-ID' => $this->deviceId,
                'Content-Type' => 'application/json'
            ]
        ];

        if (count($data) > 0) {
            $options['body'] = json_encode($data);
        }

        return HttpClient::create()->request(
            $method,
            $this->apiHost . $uri,
            $options
        )->toArray();
    }
}