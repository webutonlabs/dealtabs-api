<?php

namespace App\Infrastructure\Service;

use Doctrine\ORM\Query;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class Paginator
{
    private PaginatorInterface $paginator;
    private ?Request $request;

    public function __construct(PaginatorInterface $paginator, RequestStack $request)
    {
        $this->paginator = $paginator;
        $this->request = $request->getCurrentRequest();
    }

    public function paginate(Query $query): PaginationInterface
    {
        return $this->paginator->paginate(
            $query,
            $this->request->query->getInt('page', 1),
            $this->request->query->getInt('limit', 12)
        );
    }
}