<?php

namespace App\Infrastructure\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

class ExceptionListener
{
    public function __construct(
        private RouterInterface $router,
        private SessionInterface $session,
        private Security $security,
        private TranslatorInterface $translator
    ) {}

    public function onKernelException(ExceptionEvent $event): void
    {
        if ($_ENV['APP_ENV'] === 'dev') {
            return;
        }

        $this->handleException($event);
    }

    /**
     * @param ExceptionEvent $event
     */
    private function handleException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        if ($exception instanceof HttpExceptionInterface) {
            $code = Response::HTTP_NOT_FOUND;
            $message = "Resource was not found!";
        } else {
            $code = Response::HTTP_INTERNAL_SERVER_ERROR;
            $message = "Oops... Something went wrong...";
        }

        $this->session->set('error.code', $code);
        $this->session->set('error.message', $this->translator->trans($message));

        $response = new RedirectResponse($this->router->generate('site_error', ['_locale' => $event->getRequest()->getLocale()]));
        $event->setResponse($response);
    }
}