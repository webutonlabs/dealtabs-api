<?php

namespace App\Infrastructure\Security;

use Google\Client;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class GoogleAuthManager
{
    private string $credentialsPath;
    private string $appHost;

    public function __construct(ContainerInterface $container)
    {
        $this->appHost = $_ENV['APP_HOST'];

        $credentialsPath = $container->getParameter('kernel.project_dir') . '/config/credentials/google_credentials_local.json';
        if (!file_exists($credentialsPath)) {
            throw new NotFoundHttpException('Google credentials were not found');
        }

        $this->credentialsPath = $credentialsPath;
    }

    public function loginUrl(): string
    {
        return $this->createInstance()->createAuthUrl();
    }

    public function createInstance(): Client
    {
        $client = new Client();

        $client->setAuthConfig($this->credentialsPath);
        $client->setAccessType('offline');
        $client->setRedirectUri($this->appHost . '/google/auth-code');
        $client->setIncludeGrantedScopes(true);
        $client->addScope(
            [
                \Google_Service_People::USERINFO_EMAIL,
                \Google_Service_People::USERINFO_PROFILE
            ]
        );

        return $client;
    }
}