<?php

namespace App\Infrastructure\Security;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
interface Roles
{
    public const USER = 'ROLE_USER';
}