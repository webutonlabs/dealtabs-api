<?php

namespace App\Infrastructure\Security\Provider;

use Symfony\Component\Security\Core\User\UserInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class User implements UserInterface
{
    private string $email = '';
    private string $name = '';
    private ?string $surname = null;
    private array $roles = ['ROLE_USER'];
    private ApiCredentials $apiCredentials;

    public function __construct(ApiCredentials $apiCredentials)
    {
        $this->apiCredentials = $apiCredentials;
    }

    public function getApiCredentials(): ApiCredentials
    {
        return $this->apiCredentials;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): void
    {
        $this->surname = $surname;
    }

    public function setDataFromApi(array $data)
    {
        $this->email = $data['email'];
        $this->name = $data['name'];
        $this->surname = $data['surname'];
    }

    public function eraseCredentials()
    {
        //
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        //
    }

    public function getPassword()
    {
        //
    }
}