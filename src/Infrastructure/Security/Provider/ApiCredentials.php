<?php

namespace App\Infrastructure\Security\Provider;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class ApiCredentials
{
    public function __construct(
        private string $accessToken,
        private \DateTimeInterface $expiresAt
    ) {}

    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    public function getExpiresAt(): \DateTimeInterface
    {
        return $this->expiresAt;
    }
}