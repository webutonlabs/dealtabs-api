<?php

namespace App\Infrastructure\Security\Provider;

use App\Infrastructure\ExternalService\Api\ApiClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class UserProvider implements UserProviderInterface, PasswordUpgraderInterface
{
    private Request $request;

    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function loadUserByUsername(string $username)
    {
        $accessToken = $this->request->cookies->get('token', null);
        $accessTokenExpiresAt = $this->request->cookies->get('token-expires-at', null);

        if (null === $accessToken || null === $accessTokenExpiresAt) {
            throw new UsernameNotFoundException();
        }

        $apiCredentials = new ApiCredentials($accessToken, new \DateTimeImmutable($accessTokenExpiresAt));
        $apiClient = new ApiClient($apiCredentials->getAccessToken());

        $user = new User($apiCredentials);
        $user->setDataFromApi($apiClient->getUserData());

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $apiClient = new ApiClient($user->getApiCredentials()->getAccessToken());
        try {
            $user->setDataFromApi($apiClient->getUserData());
        } catch (\Exception $e) {
            throw new UsernameNotFoundException();
        }

        return $user;
    }

    public function supportsClass(string $class)
    {
        return $class === User::class;
    }

    public function upgradePassword(UserInterface $user, string $newEncodedPassword): void
    {
        // TODO: Implement upgradePassword() method.
    }
}