<?php

namespace App\Infrastructure\Security\Factory;

use App\Infrastructure\Security\Provider\ApiCredentials;
use Symfony\Component\HttpFoundation\Cookie;
use Ramsey\Uuid\Uuid;

class SecurityCookieFactory
{
    public static function token(ApiCredentials $apiCredentials): Cookie
    {
        return new Cookie('token', $apiCredentials->getAccessToken(),
                          $apiCredentials->getExpiresAt(), '/', $_ENV['APP_DOMAIN'], false, false);
    }

    public static function tokenExpiresAt(ApiCredentials $apiCredentials): Cookie
    {
        return new Cookie('token-expires-at', $apiCredentials->getExpiresAt()->format('c'),
                          $apiCredentials->getExpiresAt(), '/', $_ENV['APP_DOMAIN'], false, false);
    }

    public static function deviceId(): Cookie
    {
        return new Cookie('device-id', Uuid::uuid4()->toString(),
                          new \DateTimeImmutable('now + 100 years'), '/', $_ENV['APP_DOMAIN'], false, false);
    }
}