<?php

namespace App\Infrastructure\Security\Guard;

use App\Infrastructure\ExternalService\Api\ApiAuthClient;
use App\Infrastructure\Security\Factory\SecurityCookieFactory;
use App\Infrastructure\Security\Provider\ApiCredentials;
use App\Infrastructure\Security\Provider\User;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class SiteAuthenticator extends AbstractFormLoginAuthenticator
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private CsrfTokenManagerInterface $csrfTokenManager,
        private ApiAuthClient $apiAuthClient,
        private TranslatorInterface $translator
    ) {}

    public function supports(Request $request)
    {
        return 'user_login' === $request->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request)
    {
        $credentials = [
            'login' => $request->request->get('login'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['login']
        );

        return $credentials;
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);

        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new CustomUserMessageAuthenticationException('Please try again...');
        }

        try {
            $response = $this->apiAuthClient->authByUsername($credentials['login'], $credentials['password']);

            $apiCredentials = new ApiCredentials($response['access_token'], new \DateTimeImmutable($response['expires_at']));
        } catch (ClientException $e) {
            $responseContent = $e->getResponse()->toArray(false);

            if ($responseContent['message'] === 'You must confirm your email address!') {
                $this->apiAuthClient->resendConfirmationEmail($credentials['login']);
            }

            throw new CustomUserMessageAuthenticationException($this->translator->trans($responseContent['message']) . ' ' . $this->translator->trans('Confirmation email was successfully resent.'));
        }

        return new User($apiCredentials);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $redirectUrl = $this->urlGenerator->generate('user_login_succeeded');
        if ($request->getSession()->has('collaborated_session') && $request->getSession()->has('collaborated_session_confirmed')) {
            if (!$request->getSession()->get('collaborated_session_confirmed')) {
                $redirectUrl = $this->urlGenerator->generate('site_confirm_collaboration_by_session_id', [
                    'sessionId' => $request->getSession()->get('collaborated_session')
                ]);

                $request->getSession()->set('collaborated_session_confirmed', true);
            }
        }

        $response = new RedirectResponse($redirectUrl);

        /** @var User $user */
        $user = $token->getUser();

        $response->headers->setCookie(SecurityCookieFactory::token($user->getApiCredentials()));
        $response->headers->setCookie(SecurityCookieFactory::tokenExpiresAt($user->getApiCredentials()));

        if (!$request->cookies->has('device-id')) {
            $response->headers->setCookie(SecurityCookieFactory::deviceId());
        }

        return $response;
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate('user_login');
    }
}
