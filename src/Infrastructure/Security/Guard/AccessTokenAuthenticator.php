<?php

namespace App\Infrastructure\Security\Guard;

use App\Infrastructure\ExternalService\Api\ApiAuthClient;
use App\Infrastructure\Security\Factory\SecurityCookieFactory;
use App\Infrastructure\Security\Provider\ApiCredentials;
use App\Infrastructure\Security\Provider\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class AccessTokenAuthenticator extends AbstractFormLoginAuthenticator
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private CsrfTokenManagerInterface $csrfTokenManager,
        private ApiAuthClient $apiAuthClient,
        private TranslatorInterface $translator
    ) {}

    public function supports(Request $request): bool
    {
        return $request->query->has('access_token') && $request->query->has('expires_at');
    }

    public function getCredentials(Request $request)
    {
        return [
            'access_token' => $request->query->get('access_token'),
            'expires_at' => $request->query->get('expires_at')
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $apiCredentials = new ApiCredentials($credentials['access_token'], new \DateTimeImmutable($credentials['expires_at']));

        return new User($apiCredentials);
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $redirectUrl = $this->urlGenerator->generate('user_login_succeeded');
        if ($request->getSession()->has('collaborated_session') && $request->getSession()->has('collaborated_session_confirmed')) {
            if (!$request->getSession()->get('collaborated_session_confirmed')) {
                $redirectUrl = $this->urlGenerator->generate('site_confirm_collaboration_by_session_id', [
                    'sessionId' => $request->getSession()->get('collaborated_session')
                ]);

                $request->getSession()->set('collaborated_session_confirmed', true);
            }
        }

        $response = new RedirectResponse($redirectUrl);

        /** @var User $user */
        $user = $token->getUser();

        $response->headers->setCookie(SecurityCookieFactory::token($user->getApiCredentials()));
        $response->headers->setCookie(SecurityCookieFactory::tokenExpiresAt($user->getApiCredentials()));

        if (!$request->cookies->has('device-id')) {
            $response->headers->setCookie(SecurityCookieFactory::deviceId());
        }

        return $response;
    }

    protected function getLoginUrl()
    {
        return $this->urlGenerator->generate('user_login');
    }
}