<?php

namespace App\Infrastructure\EventSubscriber;

use App\Infrastructure\ExternalService\Api\ApiClient;
use App\Infrastructure\Security\Provider\ApiCredentials;
use App\Infrastructure\Security\Provider\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;

class AuthByApiTokenSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private Security $security,
        private ContainerInterface $container
    ) {}

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if (null === $this->security->getUser()) {
            if ($request->cookies->has('token') &&
                $request->cookies->has('token-expires-at') &&
                $request->cookies->has('device-id')
            ) {
                $now = (new \DateTimeImmutable('now'))->getTimestamp();
                $tokenExpires = (new \DateTimeImmutable($request->cookies->get('token-expires-at')))->getTimestamp();

                if ($tokenExpires > $now) {
                    $apiCredentials = new ApiCredentials($request->cookies->get('token'), new \DateTimeImmutable($request->cookies->get('token-expires-at')));
                    $user = new User($apiCredentials);

                    try {
                        $apiClient = new ApiClient($apiCredentials->getAccessToken());
                        $apiClient->getUserData(); // checking if token is valid => if not exception will be thrown

                        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                        $this->container->get('security.token_storage')->setToken($token);
                        $session = $this->container->get('session');
                        $session->set('_security_main', serialize($token));

                        if (null !== $request->get('_route')) {
                            $redirectUrl = $this->urlGenerator->generate($request->get('_route'), $request->get('_route_params'));

                            if (!str_starts_with($redirectUrl, '/_wdt')) {
                                $response = new RedirectResponse($redirectUrl);
                                $event->setResponse($response);
                            }
                        }
                    } catch (\Exception) {
                        $this->logout($event);
                    }
                } else {
                    $this->logout($event);
                }
            }
        }
    }

    private function logout(RequestEvent $event)
    {
        $response = new RedirectResponse($this->urlGenerator->generate('user_logout'));
        $event->setResponse($response);
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => ['onKernelRequest', 7]
        ];
    }
}