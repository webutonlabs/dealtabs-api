<?php

namespace App\Infrastructure\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Security;

class LocaleSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private RouterInterface $router,
        private Security $security
    ){}

    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if ($locale = $request->attributes->get('_locale')) {
            $request->getSession()->set('_locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', explode('_', $request->getPreferredLanguage())[0]));
        }

        $routeExists = false;
        foreach($this->router->getRouteCollection() as $routeObject) {
            $routePath = $routeObject->getPath();

            if($routePath === "/{_locale}{$request->getPathInfo()}") {
                $routeExists = true;
                break;
            }
        }

        if($routeExists) {
            $url = "/{$request->getLocale()}{$request->getPathInfo()}";

            if (count($request->query->all()) > 0) {
                $params = http_build_query($request->query->all());
                $url = "$url?$params";
            }

            $event->setResponse(new RedirectResponse($url));
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest', 34]],
        ];
    }
}