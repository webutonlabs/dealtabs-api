<?php

namespace App\Infrastructure\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\LogoutEvent;

class LogoutSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            'Symfony\Component\Security\Http\Event\LogoutEvent' => 'onLogout'
        ];
    }

    public function onLogout(LogoutEvent $event)
    {
        $event->getResponse()->headers->clearCookie('token', '/', $_ENV['APP_DOMAIN']);
        $event->getResponse()->headers->clearCookie('token-expires-at', '/', $_ENV['APP_DOMAIN']);
    }
}