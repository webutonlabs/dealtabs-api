<?php

namespace App\Infrastructure\Mailer\Emails;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class SentMessageEmail
{
    public function __construct(
        private string $name,
        private string $email,
        private string $message
    ){}

    public function getEnvelope(): Email
    {
        return (new TemplatedEmail())
            ->to(new Address($_ENV['ADMIN_EMAIL']))
            ->subject('CONTACT US MESSAGE')
            ->htmlTemplate('email/contact_us.html.twig')
            ->context(
                [
                    'name' => $this->name,
                    'user_email' => $this->email,
                    'message' => $this->message
                ]
            );
    }
}