<?php

namespace App\Infrastructure\Mailer;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;

class Mailer
{
    public function __construct(
        private MailerInterface $mailer
    ){}

    public function send(Email $email): void
    {
        $email->from(new Address($_ENV['MAILER_FROM'], 'DealTabs'));
        $this->mailer->send($email);
    }
}