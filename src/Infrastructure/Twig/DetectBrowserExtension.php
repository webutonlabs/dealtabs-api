<?php

namespace App\Infrastructure\Twig;

use Sinergi\BrowserDetector\Browser;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * xTonyApps - xtonyapps@gmail.com
 *
 * @author Anton Zakharuk (zahaton01@gmail.com)
 */
class DetectBrowserExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return [
            new TwigFunction('isSafari', [$this, 'isSafari']),
            new TwigFunction('isOpera', [$this, 'isOpera']),
            new TwigFunction('isFirefox', [$this, 'isFirefox'])
        ];
    }

    public function isSafari()
    {
        $browser = new Browser();

        return $browser->getName() === Browser::SAFARI;
    }

    public function isOpera(): bool
    {
        $browser = new Browser();

        return $browser->getName() === Browser::OPERA || $browser->getName() === Browser::OPERA_MINI;
    }

    public function isFirefox(): bool
    {
        $browser = new Browser();

        return $browser->getName() === Browser::FIREFOX || $browser->getName() === Browser::MOZILLA;
    }
}