<?php

namespace App\Cli;

use App\Domain\Entity\ExtensionReview;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GenerateReviewsCommand extends Command
{
    public function __construct(private EntityManagerInterface $em)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('reviews:generate');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $enReviews = [
            ['name' => 'John', 'message' => 'Cool extension!', 'stars' => 5],
            ['name' => 'Micheal', 'message' => 'Very fast and simple extension. Satisfied for 100%', 'stars' => 5],
            ['name' => 'Dean Smith', 'message' => 'Saved tons of my time. Thank you very much', 'stars' => 5],
            ['name' => 'Daniel Mathis', 'message' => 'Live synchronization is so useful. Will recommend you to my friends', 'stars' => 5]
        ];

        foreach ($enReviews as $enReview) {
            $review = new ExtensionReview('en');
            $review->setName($enReview['name']);
            $review->setMessage($enReview['message']);
            $review->setStars($enReview['stars']);

            $this->em->persist($review);
        }

        $this->em->flush();

        return 1;
    }
}